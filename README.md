HTML 2 Embed
--------

HTML 2, a.k.a. H2E is a tiny javascript library that prepares HTML codes to be embed in other programming languages (the main target was C).


How to Use
--------
* Download/Clone this repository
* Add our script on your page:
	```html
	<script src="html2embed.min.js"></script>
	```
* **Just use it**!


Parameters
--------

H2E has basically three parameters

* **Just a String**
```javascript
h2e("Your HTML/CSS/JS code here!");
```
If you call h2e() / html2embed() passing a single string as parameter, it'll simply compress it (removing break lines and aditional blank spaces);



* **A String + boolean**
```javascript
h2e("Your HTML/CSS/JS code here!", true);
```

If the boolean send as the second parameter is true, H2E will convert all the ~ compressed ~ string characters into charcodes.



* **A String + boolean + boolean**
```javascript
h2e("Your HTML/CSS/JS code here!", false, true);
```

If the boolean send as the third parameter is true, H2E will prepare the code to be embed on C, it means '\' will be replaced by '\\', '"' by '\"' and so on.
**Notice that you can set the second parameter (the one which controls the charcode convertion) to true/false without interfering in the "embed" feature.


[You can see a demo here](http://pablodiehl.xyz/api/html2embed/)